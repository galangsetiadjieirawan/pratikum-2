package com.galangsetiadjieirawan_10191032.belajarfragment.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.galangsetiadjieirawan_10191032.belajarfragment.fragment.Tab1Fragment;
import com.galangsetiadjieirawan_10191032.belajarfragment.fragment.Tab2Fragment;

public class TabFragmentPagerAdapter extends FragmentPagerAdapter {
    String[] title = new String[]{
            "Tab 1", "Tab 2"
    };

    public TabFragmentPagerAdapter(FragmentManager fragmentManager){
        super(fragmentManager);

    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = new Tab1Fragment();
                break;
            case 1:
                fragment = new Tab2Fragment();
                break;
            default:
                fragment = null;
                break;
        }
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position){
        return title[position];
    }

    @Override
    public int getCount() {
        return title.length;
    }

}

